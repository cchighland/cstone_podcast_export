<?php

/**
 * Defines the Podcast Export Service.
 */
class PodcastExport {

  /**
   * Creates a new instance of this class.
   */
  public static function create() {
    return new static();
  }

  /**
   * Display podcast list.
   */
  public function episodes() {

    $query = db_select('node', 'n')
      ->fields('n', ['nid', 'title', 'type', 'created'])
      ->condition('n.type', ['podcast', 'nopodcast'], 'IN')
      ->condition('n.status', 1)
      ->orderBy('n.created');

    // Join for body.

    $query->leftjoin(
      'field_data_body',
      'body',
      "n.nid = body.entity_id AND n.vid = body.revision_id"
    );

    // Joins for audio file.

    $query->leftjoin(
      'field_data_field_audio_file',
      'field_audio',
      "n.nid = field_audio.entity_id AND n.vid = field_audio.revision_id"
    );
    $query->leftjoin(
      'file_managed',
      'files',
      'field_audio.field_audio_file_fid = files.fid'
    );

    // Join for sermon series taxonomy term.

    $query->leftjoin(
      'field_data_taxonomy_vocabulary_2',
      'series_term',
      "n.nid = series_term.entity_id AND n.vid = series_term.revision_id"
    );

    // Joins for additional metadata fields.

    $query->leftjoin(
      'field_data_field_podcast_speaker',
      'field_podcast_speaker',
      "n.nid = field_podcast_speaker.entity_id AND n.vid = field_podcast_speaker.revision_id"
    );
    $query->leftjoin(
      'field_data_field_podcast_scripture',
      'field_podcast_scripture',
      "n.nid = field_podcast_scripture.entity_id AND n.vid = field_podcast_scripture.revision_id"
    );
    $query->leftjoin(
      'field_data_field_podcast_duration',
      'field_podcast_duration',
      "n.nid = field_podcast_duration.entity_id AND n.vid = field_podcast_duration.revision_id"
    );
    $query->leftjoin(
      'field_data_field_podcast_livestream',
      'field_podcast_livestream',
      "n.nid = field_podcast_livestream.entity_id AND n.vid = field_podcast_livestream.revision_id"
    );

    // Attach all fields.

    $query->fields('body', ['body_value', 'body_summary', 'body_format']);
    $query->fields('files', ['uri', 'filename']);
    $query->addField('field_podcast_speaker', 'field_podcast_speaker_value', 'speaker');
    $query->addField('field_podcast_scripture', 'field_podcast_scripture_value', 'scripture');
    $query->addField('field_podcast_duration', 'field_podcast_duration_value', 'duration');
    $query->addField('field_podcast_livestream', 'field_podcast_livestream_value', 'video');
    $query->addField('series_term', 'taxonomy_vocabulary_2_tid', 'series_tid');

    // Execute the query.

    $result = $query->execute()->fetchAll();

    array_walk($result, 'self::nodeProcess');

    // Return JSON output.

    drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
    echo json_encode($result);
  }

  /**
   * Process node results.
   */
  function nodeProcess($node) {
    if (!empty($node->body_value)) {
      $node->body = [
        'value' => $node->body_value,
        'summary' => $node->body_summary,
        'format' => ['1' => 'filtered_html', '2' => 'full_html', '4' => 'full_html', '5' => 'plain_text'][$node->body_format],
      ];
    }
    else {
      $node->body = NULL;
    }
    unset($node->body_value);
    unset($node->body_summary);
    unset($node->body_format);

    if (!empty($node->uri)) {
      $node->uri = drupal_realpath($node->uri);
    }
  }

  /**
   * Display series list.
   */
  public function series() {

    $query = db_select('taxonomy_term_data', 't')
      ->fields('t')
      ->condition('t.vid', 2)
      ->orderBy('t.weight')
      ->orderBy('t.name');

    $query->leftjoin(
      'field_data_field_series_image',
      'term_image',
      "t.tid = term_image.entity_id AND term_image.entity_type = 'taxonomy_term'"
    );
    $query->leftjoin(
      'file_managed',
      'files',
      'term_image.field_series_image_fid = files.fid'
    );

    // Attach fields.

    $query->fields('files', ['uri', 'filename']);

    // Execute the query.

    $result = $query->execute()->fetchAll();

    array_walk($result, 'self::seriesProcess');

    drupal_add_http_header('Content-Type', 'application/json; charset=utf-8');
    echo json_encode($result);
  }

  /**
   * Get additional series details and fix file URIs.
   */
  private function seriesProcess($term) {

    $term->node = $this->getSeriesNode($term->tid);

    if (!empty($term->uri)) {
      $term->uri = drupal_realpath($term->uri);
    }
  }

  /**
   * Get the related series node, if it exists.
   */
  private function getSeriesNode($tid) {

    $query = db_select('node', 'n')
      ->fields('n', ['nid', 'title', 'created'])
      ->condition('n.type', 'sermon_series')
      ->condition('n.status', 1);

    // Join for body.

    $query->leftjoin(
      'field_data_body',
      'body',
      "n.nid = body.entity_id AND n.vid = body.revision_id"
    );

    // Join for sermon series taxonomy term.

    $query->join(
      'field_data_taxonomy_vocabulary_2',
      'series_term',
      "n.nid = series_term.entity_id AND n.vid = series_term.revision_id"
    );

    // Joins for main image.

    $query->leftjoin(
      'field_data_field_page_image',
      'page_image',
      "n.nid = page_image.entity_id AND n.vid = page_image.revision_id"
    );
    $query->leftjoin(
      'file_managed',
      'files',
      'page_image.field_page_image_fid = files.fid'
    );

    // Set additional query conditions.

    $query->condition('series_term.taxonomy_vocabulary_2_tid', $tid);

    // Attach fields.

    $query->fields('body', ['body_value', 'body_summary', 'body_format']);
    $query->fields('files', ['uri', 'filename']);

    $result = $query->execute()->fetchAll();

    array_walk($result, 'self::nodeProcess');

    if ($result) {
      return reset($result);
    }
  }
}
